/* Template for class BigIntExtended (Assign 2).
 * Updated on 31 October 2018.
 * This class is intended to extend the functionality provided 
 * by class BigInt.
 * This template is provided as part of Assignment 2 for COMP333.
 * Every JUnit test routine used in marking each method you write
 * will conform to the stated precondition and description of
 * output for the method.
 */
 package comp333;

public class BigIntExtended {
	
	// Euclid's algorithm, extended form.
	// Pre: a and b are positive integers.
	// Returns: triple (d, x, y) such that
	//          d = gcd(a,b) = ax + by.
	public static int [] egcd(int a, int b) {
		if (b == 0)
            return new int[]{a,1,0};
            
        int[] result = egcd(b, a%b);
        int d = result[0];
        int p = result[2];
        int q = result[1] - (a/b) * result[2];
        
        return new int[] {d,p,q};
	}
	
	// Modular inversion.
	// Pre: a and n are positive integers which are coprime.
	// Returns: the inverse of a modulo n.
	//          The inverse of a modulo n should be an element
	//          of Z_n = {0,1,...,n-1}.
	public static int minv(int a, int n) {
		int n_tmp = n;
		if (n == 0) 
            return 1;
		int[] result = egcd(a,n);
		if (result[1] < 0)
            return result[1] += n_tmp;
		return result[1];
	}
	
	// Chinese remainder algorithm.
	// Pre: p and q are different prime numbers.
	//      0 <= a < p and 0 <= b < q.
	// Returns: the unique integer c such that
	//          c is congruent to a modulo p,
	//          c is congruent to b modulo q,
	//          and 0 <= c < pq.
	public static int cra(int p, int q, int a, int b) {
		int c = 0;
		int product = p*q;
		int partialProduct_p = product/p;
		int partialProduct_q = product/q;
		int sum = partialProduct_p * minv(partialProduct_p, p) * a;
		sum += partialProduct_q * minv(partialProduct_q, q) * b;
		c = sum % product;
		
		
		return c;
	}

	public static int modular_standard(int a, int b, int n){
	    int result = 1;
        while (b > 0){
            if ((b & 1) == 1)
                result = (result * a)%n;

            b = b >> 1;
            a = (a*a) % n;
        }
        return result;
    }

	// Modular exponentiation.
	// Pre: a and b are nonnegative big integers.
	//      n is a positive big integer.
	// Returns: this method returns a^b mod n.
	public static int modexp(int a, int b, int n) {
		//int result = 1;
		a = a % n ;
        return modular_standard(a,b,n);
    }
	
	// Modular exponentiation, version 2.
	// This method returns a^b mod n, in case n is prime.
	// (An improvement to the standard efficient mod exp
	// algorithm is requested for this special case.
	// As a hint, try to reduce the size of the exponent b
	// right at the beginning.)
	public static int modexpv2(int a, int b, int n) {
        //int result = 1;
		if (b == n)
		    return a;
		else if (b > n+1)
		    b = b - n + 1;
        if (a<n) {
            if (b == (n - 1))
                return 1;
            else if (b > (n - 1))
                b = b - n - 1;
        }
        return modular_standard(a,b,n);
		// Complete this code.
	}
	
	// Modular exponentiation, version 3.
	// This method returns a^b mod n, in case n = pq, for different
	// primes p and q.
	// (An improvement to the standard efficient mod exp
	// algorithm is requested for this special case.
	// As a hint, use the Chinese remainder algorithm.
		public static int modexpv3(int a, int b, int p, int q) {
	        int d = 0;
			
			// Complete this code.
			
			return d;
	}
		
	// Pre: a and b are nonnegative big integers of equal length.
	// Returns: the product of a and b using karatsuba's algorithm.
	public static BigInt karatsuba(BigInt a, BigInt b) {
		BigInt result = new BigInt();
		
		// Complete this code.
		
		return result;
	}
	
	// Pre: n is an odd big integer greater than 4.
	//      s is an ordinary positive integer.
	// Returns: if n is prime then returns true with certainty,
	//          otherwise returns false with probability
	//          1 - 4^{-s}.
	public static boolean millerrabin(BigInt n, int s) {
	
		// Complete this code.
		
		return true;
	}
	
	// Pre: l and s are ordinary positive integers.
	// Returns: a random "probable" big prime number n of length l decimal digits.
	//          The probability that n is not prime is less than 4^{-s}.
	public static BigInt randomprime(int l, int s) {
		BigInt result = new BigInt();
		
		// Complete this code.
		
		return result;
	}

	public static void main(String[] args) {
		
		// Provide a simple interactive demo of the RSA
		// cryptosystem. You may use ordinary Java int
		// values in your demo. (that is, you do not have to
		// use big integers.
	}
	
	
}
