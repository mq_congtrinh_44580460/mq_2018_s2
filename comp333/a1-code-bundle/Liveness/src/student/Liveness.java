/*
 * By 
 *Rifhad Mahbub (44647662)
 *Cong Trinh (44580460)
 * COMP333 MQ
 */

package student;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.LinkedList;


/*
 *https://www.geeksforgeeks.org/graph-coloring-set-2-greedy-algorithm/
 *Graph implementation and graph colouring
*/
class Graph
{
	private int V;   // No. of vertices
	private LinkedList<Integer> adj[]; //Adjacency List

	//Constructor
	Graph(int v)
	{
		V = v;
		adj = new LinkedList[v];
		for (int i=0; i<v; ++i)
			adj[i] = new LinkedList();
	}

	//Function to add an edge into the graph
	void addEdge(int v,int w)
	{
		adj[v].add(w);
		 //Graph is undirected
	}

	// Assigns colors (starting from 0) to all vertices and
	// prints the assignment of colors
	TreeMap<String, Integer> greedyColoring(ArrayList<String> varNames) {
		TreeMap<String, Integer> sol = new TreeMap<String, Integer>();
		int result[] = new int[V];

		// Initialize all vertices as unassigned
		Arrays.fill(result, -1);

		// Assign the first color to first vertex
		result[0]  = 0;

		// A temporary array to store the available colors. False
		// value of available[cr] would mean that the color cr is
		// assigned to one of its adjacent vertices
		boolean available[] = new boolean[V];

		// Initially, all colors are available
		Arrays.fill(available, true);

		// Assign colors to remaining V-1 vertices
		for (int u = 1; u < V; u++)
		{
			// Process all adjacent vertices and flag their colors
			// as unavailable
			Iterator<Integer> it = adj[u].iterator() ;
			while (it.hasNext())
			{
				int i = it.next();
				if (result[i] != -1)
					available[result[i]] = false;
			}

			// Find the first available color
			int cr;
			for (cr = 0; cr < V; cr++){
				if (available[cr])
					break;
			}

			result[u] = cr; // Assign the found color

			// Reset the values back to true for the next iteration
			Arrays.fill(available, true);
		}

		// print the result
		for (int i = 0; i < V; i++)
			sol.put(varNames.get(i), result[i]+1);
		return sol;
	}
}

class IntegerPair {
	int[] pair = new int[2];

	public IntegerPair(int n1, int n2) {
		pair[0] = n1;
		pair[1] = n2;
	}

	public int[] getPair() {
		return this.pair;
	}

	public void setFirst(int n) {
		pair[0] = n;
	}

	public void setLast(int n) {
		pair[1] = n;
	}

	public boolean overlapsOtherPair(IntegerPair otherPair) {
		if (this.pair[0] <= otherPair.pair[1] && otherPair.pair[0] <= this.pair[1]) {
			return true;
		}
		return false;
	}
}

class sortByValues implements Comparator<Map.Entry<String, ArrayList<IntegerPair>>>
{
	// Used for sorting 
   //    In ascending order of variables' start time
	//    In descending order of variables' end time if start time is same
	public int compare(Map.Entry<String, ArrayList<IntegerPair>> a, Map.Entry<String, ArrayList<IntegerPair>> b)
	{
		ArrayList<IntegerPair> first = a.getValue();
		ArrayList<IntegerPair> second = b.getValue();
		if (first.get(first.size()-1).pair[0] != second.get(second.size()-1).pair[0])
			return first.get(first.size()-1).pair[0] - second.get(second.size()-1).pair[0];
		else
			return second.get(second.size()-1).pair[1] - first.get(first.size()-1).pair[1];
	}
}

//Main Liveness class.
public class Liveness {


	public TreeMap<String, Integer> generateSolution(String fInName) {
		// PRE: fInName is a valid input file
		// POST: returns a TreeMap mapping variables (String) to registers (Integer)
		TreeMap<String, Integer> soln = new TreeMap<String, Integer>();
		TreeMap<String, ArrayList<IntegerPair>> variableLines = new TreeMap<String, ArrayList<IntegerPair>>(); 
		// A treemap of the variables mapped to an array of integers for the lines at which they start and then end.

		// File opening and reading.
		ArrayList<String> fileLines = getFileStrings(fInName);

		// Do liveness analysis.
		variableLines = getLivenessAnalysisFor(fileLines);
		
		//Convert TreeMap to LinkedList to sort 
		List<Map.Entry<String, ArrayList<IntegerPair>>> list = new LinkedList<Map.Entry<String, ArrayList<IntegerPair>>>(variableLines.entrySet());
		
		Collections.sort(list, new sortByValues());
		
		//printLivenessAnalysis(variableLines); //DEBUG PRINT.
		
		//Convert LinkedList to Map 
		Map<String, ArrayList<IntegerPair>> sortedMap = new LinkedHashMap<String, ArrayList<IntegerPair>>();

		for (Map.Entry<String, ArrayList<IntegerPair>> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		ArrayList<String> variableNames = getArrayListOfKeysFromSorted(sortedMap);
		Graph g = new Graph(variableNames.size());
		for (int i = 0 ; i < variableNames.size(); i++){
			String currentVarI = variableNames.get(i);
			for (int j = 0 ; j < variableNames.size(); j++){
				String currentVarJ = variableNames.get(j);
				if (currentVarI == currentVarJ){
					break;
				}
				else if (doVariablesCollide(variableLines, currentVarI, currentVarJ)){
					g.addEdge(i,j);
				}
			}
		}
		soln = g.greedyColoring(variableNames);
		//printRegisterSolution(soln); // DEBUG PRINT.
		return soln;
	}




	public void writeSolutionToFile(TreeMap<String, Integer> t, String solnName) {
		// PRE: t represents a valid register allocation
		// POST: the register allocation in t is written to file solnName

		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(solnName));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		int numRegisters = getMaxNumberRegister(t);
		try {
			writer.write(Integer.toString(numRegisters) + "\n");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Iterator<String> keyIter = t.keySet().iterator();
		while (keyIter.hasNext()) {
			String variableName = keyIter.next();
			String line = variableName + " " + Integer.toString(t.get(variableName)) + "\n";
			try {
				writer.write(line);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static void main(String[] args)
	{

		Liveness n = new Liveness();
		TreeMap<String, Integer> soln = n.generateSolution("/home/mrrobot1802/mq_2018_s2/comp333/a1-code-bundle/Data files for Assignment 1-20180922/ex15.dat");
		n.writeSolutionToFile(soln, "/home/mrrobot1802/mq_2018_s2/comp333/a1-code-bundle/Data files for Assignment 1-20180922/mytest.out");

	}

	/**
	 * Liveness mapping logic method.
	 */
	// Performs liveness analysis on the given string arraylist.
	// Returns a TreeMap of the variable name as the key to an integer pair (the liveness analysis values). A key may have multiple integer pairs.
	private TreeMap<String, ArrayList<IntegerPair>> getLivenessAnalysisFor(ArrayList<String> fileLines) {
		TreeMap<String, ArrayList<IntegerPair>> variableLines = new TreeMap<String, ArrayList<IntegerPair>>(); // A treemap of the variables mapped to an array of integers for the lines at which they start and then end.
		// Go through saved lines backwards, mapping variables to their liveness analysis values.
		for (int currentLineNum = fileLines.size() - 1; currentLineNum >= 0; currentLineNum--) {
			String[] words = fileLines.get(currentLineNum).split(" "); // words in the current line.

			// Last line case.
			if (words[0].equals("live-out")) {
				for (int i = 1; i < words.length; i++) {
					ArrayList<IntegerPair> allPairs = new ArrayList<IntegerPair>();
					IntegerPair ip = new IntegerPair(-1, currentLineNum + 1);
					allPairs.add(ip);
					variableLines.put(words[i], allPairs);
				}
			}
			// First line case.
			else if (words[0].equals("live-in")) {
				for (int i = 1; i < words.length; i++) {
					ArrayList<IntegerPair> allPairs = variableLines.get(words[i]);
					allPairs.get(allPairs.size() - 1).setFirst(currentLineNum + 1);
					variableLines.put(words[i], allPairs);
				}
			}
			// Expression or memory case.
			else {
				boolean lhs = true;
				for (int i = 0; i < words.length; i++) {
					//System.out.println(words[i]);
					// Determine whether LHS OR RHS.
					if (words[i].equals(":=")) {
						lhs = false;
						continue;
					}
					// LHS. (if on lhs assume already exists).
					if (lhs && isVariable(words[i])) {
						ArrayList<IntegerPair> allPairs = variableLines.get(words[i]);
						allPairs.get(allPairs.size() - 1).setFirst(currentLineNum + 1);
						variableLines.put(words[i], allPairs);
					}
					// RHS.
					else if (!lhs && isVariable(words[i])) {
						ArrayList<IntegerPair> allPairs = new ArrayList<IntegerPair>();
						// Could already exist.
						if (variableLines.containsKey(words[i])) {
							allPairs = variableLines.get(words[i]);
							// Exists and is still being used.
							if (allPairs.get(allPairs.size() - 1).getPair()[0] == -1) {
								continue;
							}
						}
						IntegerPair ip = new IntegerPair(-1, currentLineNum + 1);
						allPairs.add(ip);
						variableLines.put(words[i], allPairs);
					}
					// RHS: If memory string, check expression.
					else if (!lhs && words[i].contains("mem")) {
						// Merge all words into expression in the line if memory call.
						String mergeWord = words[i];
						for (int j = i + 1; j < words.length; j++) {
							mergeWord += words[j];
						}
						ArrayList<String> memVariables = getVariablesInExpression(mergeWord.substring(4));
						for (int j = 0; j < memVariables.size(); j++) {
							// For every register in memory expression.
							//System.out.println("memRegister: " + memVariables.get(j));
							ArrayList<IntegerPair> allPairs = new ArrayList<IntegerPair>();
							// Could already exist.
							if (variableLines.containsKey(memVariables.get(j))) {
								allPairs = variableLines.get(memVariables.get(j));
								// Exists and is still being used.
								if (allPairs.get(allPairs.size() - 1).getPair()[0] == -1) {
									continue;
								}
							}
							IntegerPair ip = new IntegerPair(-1, currentLineNum + 1);
							allPairs.add(ip);
							variableLines.put(memVariables.get(j), allPairs);
						}
						break; // Go to next line now since memory case (no words left).
					}
				}
			}
		}
		return variableLines;
	}


	/**
	 * Helper methods.
	 */
	// Returns whether the given string is a variable or not as defined in the assignment specs. [a-Z]([a-Z]|[0-9])
	public boolean isVariable(String name) {
		if (name.length() == 2 && isLetter(name.charAt(0)) && (isLetter(name.charAt(1)) || isDigit(name.charAt(1)))) {
			return true;
		}
		return false;
	}

	public boolean isLetter(char c) {
		if (Character.isLetter(c)) {
			return true;
		}
		return false;
	}

	public boolean isDigit(char c) {
		int charNum = Character.getNumericValue(c);
		if (charNum >= 0 && charNum <= 9) {
			return true;
		}
		return false;
	}

	// Will return array of registers (as string names) from the given expression.
	public ArrayList<String> getVariablesInExpression(String expression) {
		ArrayList<String> registers = new ArrayList<String>();
		int indexPos = 0;
		while (indexPos < expression.length()) {
			String checkString = expression.substring(indexPos, indexPos + 2);
			// Is a variable.
			if (isVariable(checkString)) {
				registers.add(checkString);
				indexPos += 3;
			}
			// Not a register (constant digit). Skip to next variable/digit.
			else {
				char[] skipChars = {'+', '-', '*', '/', ']'};
				while (indexPos < expression.length() && !charArrayContains(skipChars, expression.charAt(indexPos))) {
					indexPos++;
				}
				indexPos++;
			}
			//System.out.println("");
		}

		return registers;
	}

	// Returns whether two given variables in the given treeMap have colliding intergerpair ranges.
	private boolean doVariablesCollide(TreeMap<String, ArrayList<IntegerPair>> treeMap, String variable1, String variable2) {
		ArrayList<IntegerPair> variable1Pairs = treeMap.get(variable1);
		ArrayList<IntegerPair> variable2Pairs = treeMap.get(variable2);

		for (int i = 0; i < variable1Pairs.size(); i++) {
			for (int j = 0; j < variable2Pairs.size(); j++) {
				if (variable1Pairs.get(i).overlapsOtherPair(variable2Pairs.get(j))) {
					return true;
				}
			}
		}
		return false;
	}

	// Returns the highest numbered register in the given solution mapping (variables to register numbers).
	private int getMaxNumberRegister(TreeMap<String, Integer> treeMap) {
		int maxVal = -1;
		Iterator<String> keyIter = treeMap.keySet().iterator();
		while (keyIter.hasNext()) {
			int checkVal = treeMap.get(keyIter.next());
			if (checkVal > maxVal) {
				maxVal = checkVal;
			}
		}
		return maxVal;
	}

	// Returns the most used (in terms of total line range) register number in the given set of registers.
	private int getMostUsedRegister(ArrayList<Integer> validRegisters, TreeMap<String, Integer> variableRegisters, TreeMap<String, ArrayList<IntegerPair>> variableLines) {
		// Build TreeMap, mapping registers to their usage.
		TreeMap<Integer, Integer> registerUsage = new TreeMap<Integer, Integer>(); // Register, usageamount.

		Iterator<String> variableRegistersIter = variableRegisters.keySet().iterator();
		while (variableRegistersIter.hasNext()) {
			String currentVariable = variableRegistersIter.next();
			int currentRegister = variableRegisters.get(currentVariable);
			if (!validRegisters.contains(currentRegister)) {
				continue;
			}

			int currentUsage = 0;
			// Iterate through the current variable line pairs, and add the ranges together.
			for (int i = 0; i < variableLines.get(currentVariable).size(); i++) {
				IntegerPair currentPair = variableLines.get(currentVariable).get(i);
				currentUsage += (currentPair.pair[1] - currentPair.pair[0]);
			}

			// Register already had variable line sum, so let's add to it.
			if (registerUsage.get(currentRegister) != null) {
				currentUsage += registerUsage.get(currentRegister);
			}
			registerUsage.put(currentRegister, currentUsage);
		}


		// Find the most used register based on built TreeMap above.
		Iterator<Integer> registerIter = registerUsage.keySet().iterator();
		int mostUsedRegister = registerIter.next();
		while (registerIter.hasNext()) {
			int currentRegister = registerIter.next();
			int currentUsage = registerUsage.get(currentRegister);
			if (currentUsage > registerUsage.get(mostUsedRegister)) {
				mostUsedRegister = currentRegister;
			}
		}
		return mostUsedRegister;
	}


	/**
	 * Toolbox methods.
	 */
	// Returns true if the given character is found in the array. Otherwise, returns false.
	private boolean charArrayContains(char[] arr, char c) {
		for (int i = 0 ; i < arr.length; i++) {
			if (arr[i] == c) {
				return true;
			}
		}
		return false;
	}

	// Returns an ArrayList of String type containing the keys of the given TreeMap.

	private ArrayList<String> getArrayListOfKeysFromSorted(Map<String, ArrayList<IntegerPair>> map){
		ArrayList<String> keys = new ArrayList<String>();
		Iterator<String> iter = map.keySet().iterator();
		while (iter.hasNext()) {
			keys.add(iter.next());
		}
		return keys;
	}
	private ArrayList<String> getArrayListOfKeys(TreeMap<String, ArrayList<IntegerPair>> treeMap) {
		ArrayList<String> keys = new ArrayList<String>();
		Iterator<String> iter = treeMap.keySet().iterator();
		while (iter.hasNext()) {
			keys.add(iter.next());
		}
		return keys;
	}

	// Returns an ArrayList of String type containing each line (from line 0 to n) of the given path's file.
	private ArrayList<String> getFileStrings(String filePath) {
		// File opening and reading.
		File f = new File(filePath);
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(f));
		} catch (FileNotFoundException e1) {
			System.out.println("File not found: " + filePath);
			e1.printStackTrace();
		}

		// Start reading file, and save into array.
		String currentLine;
		ArrayList<String> fileLines = new ArrayList<String>();
		try {
			while ((currentLine = reader.readLine()) != null) {
				fileLines.add(currentLine);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileLines;
	}

	// Prints the variable to liveness line pairs mapping.
	private void printLivenessAnalysis(TreeMap<String, ArrayList<IntegerPair>> variableLines) {
		System.out.println("PRINTING LIVENESS ANALYSIS");
		Iterator<String> keys = variableLines.keySet().iterator();
		for (int i = 0; i < variableLines.size(); i++) {
			String variableName = keys.next();
			System.out.println("Variable: " + variableName); // Variable name.
			for (int j = 0; j < variableLines.get(variableName).size(); j++) {
				ArrayList<IntegerPair> ip = variableLines.get(variableName);
				System.out.println("Lines: " + ip.get(j).pair[0] + ", " + ip.get(j).pair[1]);
			}
			System.out.println("");
		}
		System.out.println("Done.");
	}

	// Prints every variable and the register it is mapped to.
	private void printRegisterSolution(TreeMap<String, Integer> soln) {
		System.out.println("PRINTING REGISTER SOLUTION");
		Iterator<String> keys = soln.keySet().iterator();
		while (keys.hasNext()) {
			String currentKey = keys.next();
			System.out.println("Variable " + currentKey + ", at register: " + soln.get(currentKey));
		}
		System.out.println("RegisterCount: " + getMaxNumberRegister(soln));
	}

	// Returns an arraylist of integers from 1 to the given paremeter.
	private ArrayList<Integer> getNaturalSetTo(int maxVal) {
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		for (int i = 0; i < maxVal; i++) {
			numbers.add(i + 1);
		}
		return numbers;
	}


}
