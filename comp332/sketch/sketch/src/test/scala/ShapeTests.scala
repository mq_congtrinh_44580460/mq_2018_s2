import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ShapeTests extends FunSuite {

  import Geometry._

  test ("The Rectangle area method should give zero for a minimal rectangle") {
      val r = new Rectangle ((10, 10), (10, 10))
      assertResult (0.0) (r.area)
  }

  test ("The Rectangle area method should give correct area for a normal rectangle") {
      val r = new Rectangle ((10, 10), (20, 40))
      assertResult (300.0) (r.area)
  }

  test ("The Rectangle contains method should say yes for a point on the border of a rectangle") {
      val r = new Rectangle ((20, 10), (30, 20))
      assertResult (true) (r.contains ((30, 15)))
  }

  test ("The Rectangle contains method should say yes for a point inside a rectangle") {
      val r = new Rectangle ((20, 10), (30, 20))
      assertResult (true) (r.contains ((22, 15)))
  }

  test ("The Rectangle contains method should say no for a point not in a rectangle") {
      val r = new Rectangle ((20, 10), (30, 20))
      assertResult (false) (r.contains ((40, 40)))
  }

  test ("The Circle area method should give zero for a minimal circle") {
      val c = new Circle ((10, 10), 0)
      assertResult (0.0) (c.area)
  }

  test ("The Circle area method should give correct area for a normal circle") {
      val c = new Circle ((10, 10), 20)
      assertResult (314.1592653589793) (c.area)
  }

  test ("The Circle contains method should say yes for a point on the border of a circle") {
      val c = new Circle ((10, 10), 20)
      assertResult (true) (c.contains ((20, 10)))
  }

  test ("The Circle contains method should say yes for a point inside a circle") {
      val c = new Circle ((10, 10), 20)
      assertResult (true) (c.contains ((12, 12)))
  }

  test ("The Circle contains method should say no for a point not in a circle") {
      val c = new Circle ((10, 10), 20)
      assertResult (false) (c.contains ((40, 40)))
  }

}
