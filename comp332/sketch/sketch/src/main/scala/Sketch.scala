import processing.core.PApplet

class Sketch extends PApplet {

  import Geometry.pointToString

  // Standard colours
  val BLACK = 0
  val BLUE = color (0, 0, 255)
  val RED = color (255, 0, 0)
  val WHITE = 255

  // List of currently drawn shapes
  var shapes : List[Shape] = Nil

  // True if we are drawing shapes, false if selecting
  var drawMode = true

  // In draw mode, the builder for new shapes
  var builder : ShapeBuilder = Rectangle

  // Colour with which to fill subsequent shapes
  var fillColour = WHITE

  // In select mode, the currently selected shape
  var selectedShape : Shape = defaultShape

  // A default shape to use if there is no selection
  def defaultShape =
    new Rectangle ((0, 0), (width, height))

  // State of mouse dragging, beginning and ending of drag
  var mouseDragging = false
  var mouseBegin = (0, 0)
  var mouseEnd = (0, 0)

  // Positions of the various labels
  def areaX = width - 160
  def areaY = height - 20
  def dragX = width - 130
  def dragY = modeY
  val modeX = 10
  val modeY = 20

  /**
   * Start the applet running
   */
  def start (args : Array[String]) {
    PApplet.main ("Sketch" +: args)
  }

  /**
   * Called once when the applet starts.
   */
  override def setup () {
    size (400, 400)
  }

  /**
   * Called repeatedly by event loop.
   */
  override def draw () {

    // Clear the background
    background (WHITE)

    // Draw each of the current shapes
    stroke (BLUE)
    for (shape <- shapes) {
      shape.draw (this)
    }

    // Draw the area of the most recently selected shape
    fill (BLACK)
    text ("Area = " + f"${selectedShape.area}%.2f", areaX, areaY)
    
    // Draw the circumference of the most recently selected shapes
    fill (BLACK)
    text ("Circumference = " + f"${selectedShape.circumference}%.2f", areaX, areaY + 10)

    // Draw the mode indicator
    val mode = if (drawMode)
                 s"Draw ${builder.description}"
               else
                 "Select"
    text (mode, modeX, modeY)

    // If we are dragging the mouse, show the end points
    if (mouseDragging) {
      stroke (RED)
      line (mouseBegin._1, mouseBegin._2, mouseEnd._1, mouseEnd._2)
      text (pointToString (mouseBegin) + "-" + pointToString (mouseEnd), dragX, dragY)
    }

  } // End of draw

  // Mouse handling

  /**
   * Called each time the mouse button is pressed down. If in draw mode,
   * record the beginning point of the drag.
   */
  override def mousePressed () {
    if (drawMode) {
      mouseDragging = true
      mouseBegin = (mouseX, mouseY)
      mouseEnd = mouseBegin
    }
  }

  /**
   * Called each time the mouse button is moved. If in draw mode, update
   * the end point of the current drag.
   */
  override def mouseDragged () {
    if (drawMode) {
      mouseEnd = (mouseX, mouseY)
    }
  }

  /**
   * Called each time the mouse button is released. If in draw mode, stop
   * the drag in progress, build a new shape using the dragged points and
   * add the new shape to the shapes list.
   */
  override def mouseReleased () {
    if (drawMode) {
      mouseDragging = false
      val shape = builder.build (mouseBegin, mouseEnd, fillColour)
      shapes = shapes :+ shape
    } else {
      updateSelection ((mouseX, mouseY))
    }
  }

  // Keyboard handling

  /**
   * Called whenever a key is pressed. `key` tells us which one.
   */
  override def keyPressed () {
    key match {
      case 'c' => drawMode = true
                  builder = Circle
      case 'r' => drawMode = true
                  builder = Rectangle
      case 's' => drawMode = false
      case 'z' => shapes = Nil
      case 'B' => fillColour = BLUE
      case 'R' => fillColour = RED
      case 'W' => fillColour = WHITE
      case _   => // Do nothing
    }
  }

  /**
   * Update the selected shape to be the frontmost one containing `pt`.
   */
  def updateSelection (pt : (Int, Int)) {

    /**
     * Find the first shape containing `pt`, return a rectangle of the whole
     * window if there is no more specific shape.
     */
    def findSelectedShape : Shape = {
      for (shape <- shapes.reverse) {
        if (shape.contains (pt))
          return shape
      }
      defaultShape
    }

    selectedShape = findSelectedShape
  }

}
