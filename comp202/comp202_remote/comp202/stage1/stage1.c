#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

struct stage1{
    int recess;
    double haircut;
    short int police;
    long scene;
    long owl;
    int flowers;
    char pot;
    unsigned char end;
    unsigned char advertisement;
    int bells;
    unsigned char mountain;
    int drop;
    char muscle[9];
    unsigned int beginner;
    float trucks;
    float advice;
};

void printFields(){
    printf("recess, ");
    printf("haircut, ");
    printf("police, ");
    printf("scene, ");
    printf("owl, ");
    printf("flowers, ");
    printf("pot, ");
    printf("end, ");
    printf("advertisement, ");
    printf("bells, ");
    printf("mountain, ");
    printf("drop, ");
    printf("muscle, ");
    printf("beginner, ");
    printf("trucks, ");
    printf("advice\n");
}

void printInit(struct stage1 *player){
    printf("%li, ", player->recess);
    printf("%f, ", player->haircut);
    printf("%d, ", player->police);
    printf("%li, ", player->scene);
    printf("%lx, ", player->owl);
    printf("%lu, ", player->flowers);
    printf("%c, ", player->pot);
    printf("%i, ", player->end);
    printf("%i, ", player->advertisement);
    printf("%lu, ", player->bells);
    printf("%i, ", player->mountain);
    printf("%li, ", player->drop);
    printf("%s, ", player->muscle);
    printf("%lu, ", player->beginner);
    printf("%f, ", player->trucks);
    printf("%f\n", player->advice);
}

/**
void usage()
{
    fprintf (stderr, "Usage: myprog infile\n");
    fprintf (stderr, "infile is a binary file\n");
    exit(1); // Failure
}
**/

int main(int argc, const char * argv[]) {
     struct stage1 player = {1, 0.000051, 0xD9, 0153, -243, 1, 'F', 1, 1, 0, 24, 0251, "ghost", 1605, 113707.445312, 0.505928};
     printFields();
     printInit(&player);
     return 0;
}
