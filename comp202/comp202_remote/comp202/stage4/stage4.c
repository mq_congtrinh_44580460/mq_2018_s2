#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>
#include <string.h>

struct stage4{
    int recess;
    double haircut;
    short int police;
    long scene;
    long owl;
    int flowers;
    char pot;
    unsigned char end;
    unsigned char advertisement;
    int bells;
    unsigned char mountain;
    int drop;
    char muscle[9];
    unsigned int beginner;
    float trucks;
    float advice;
};

struct stage4_input{
    short recess;
    float haircut;
    int police;
    unsigned int scene;
    double owl;
    char flowers;
    char pot;
    unsigned char end;
    unsigned char advertisement;
    int bells;
    short mountain;
    int drop;
    char muscle[9];
    short beginner;
    double trucks;
    double advice;
};

//Use to check the input and output
void printFile(struct stage4 *player){
    printf("%d, ", player->recess);
    printf("%f, ", player->haircut);
    printf("%i, ", player->police);
    printf("%lu, ", player->scene);
    printf("%li, ", player->owl);
    printf("%d, ", player->flowers);
    printf("%c, ", player->pot);
    printf("%i, ", player->end);
    printf("%i, ", player->advertisement);
    printf("%u, ", player->bells);
    printf("%i, ", player->mountain);
    printf("%i, ", player->drop);
    printf("%s, ", player->muscle);
    printf("%d, ", player->beginner);
    printf("%f, ", player->trucks);
    printf("%f\n", player->advice);
}

//Read file and return number of records.
int readFile(FILE *file, struct stage4_input *input[]){
    int count = 0;
    short booleans = 0;
    char ch;
    while (1){
        input[count] = (struct stage4_input*)malloc(sizeof(struct stage4_input));
        fread(&input[count]->recess, sizeof(short), 1, file);
        fread(&input[count]->haircut, sizeof(float), 1, file);
        fread(&input[count]->police, sizeof(int), 1, file);
        fread(&input[count]->scene, sizeof(int), 1, file);
        fread(&input[count]->owl, sizeof(double), 1, file);
        fread(&input[count]->flowers, sizeof(char), 1,file);
        fread(&input[count]->pot, sizeof(char), 1, file);
        fread(&booleans, sizeof(short), 1, file);
        switch (booleans){
            case 0:
                input[count]->end = 0;
                input[count]->advertisement = 0;
                input[count]->bells = 0;
                break;
            case 256:
                input[count]->end = 0;
                input[count]->advertisement = 1;
                input[count]->bells = 0;
                break;
            case 512:
                input[count]->end = 1;
                input[count]->advertisement = 0;
                input[count]->bells = 0;
                break;
            case 768:
                input[count]->end = 1;
                input[count]->advertisement = 1;
                input[count]->bells = 0;
                break;
            case 16384:
                input[count]->end = 0;
                input[count]->advertisement = 0;
                input[count]->bells = 1;
                break;
            case 16640:
                input[count]->end = 0;
                input[count]->advertisement = 1;
                input[count]->bells = 1;
                break;
            case 16896:
                input[count]->end = 1;
                input[count]->advertisement = 0;
                input[count]->bells = 1;
                break;
            case 17152:
                input[count]->end = 1;
                input[count]->advertisement = 1;
                input[count]->bells = 1;
                break;
            default:
                printf("WRONG\n");
                break;
        }
        //fread(&input[count]->end, sizeof(unsigned char), 1, file);
        //fread(&input[count]->advertisement, sizeof(unsigned char), 1, file);
        //fread(&input[count]->bells, sizeof(int), 1, file);
        fread(&input[count]->mountain, sizeof(short), 1, file);
        fread(&input[count]->drop, sizeof(int), 1, file);
        fread(&input[count]->muscle, sizeof(char[7]), 1, file);
        fread(&input[count]->beginner, sizeof(short), 1, file);
        fread(&input[count]->trucks, sizeof(double), 1, file);
        fread(&input[count]->advice, sizeof(double), 1, file);
        //printFile(input[count]);
        if (feof(file)){
            break;
        }
        count++;
    }
    fclose(file);
    return count;
}

void convertInput(struct stage4 *mybuf[], struct stage4_input *input[], int size){
    for (int i = 0 ; i < size ; i++){
        mybuf[i] = (struct stage4*)malloc(sizeof(struct stage4));
        mybuf[i]->recess = input[i]->recess;
        mybuf[i]->haircut = input[i]->haircut;
        mybuf[i]->police = input[i]->police;
        mybuf[i]->scene = input[i]->scene;
        mybuf[i]->owl = input[i]->owl;
        mybuf[i]->flowers = input[i]->flowers;
        mybuf[i]->pot = input[i]->pot;
        mybuf[i]->end = input[i]->end;
        mybuf[i]->advertisement = input[i]->advertisement;
        mybuf[i]->bells = input[i]->bells;
        mybuf[i]->mountain = input[i]->mountain;
        mybuf[i]->drop = input[i]->drop;
        strcpy(mybuf[i]->muscle, input[i]->muscle);
        mybuf[i]->beginner = input[i]->beginner;
        mybuf[i]->trucks = input[i]->trucks;
        mybuf[i]->advice = input[i]->advice;
        //printFile(mybuf[i]);
    }
}

void writeFile(FILE *fileWrite, struct stage4 *mybuf[], int size){
    for (int i = 0 ; i < size ; i++){
        fwrite(&mybuf[i]->recess, sizeof(int), 1, fileWrite);
        fwrite(&mybuf[i]->haircut, sizeof(double), 1, fileWrite);
        fwrite(&mybuf[i]->police, sizeof(short int), 1, fileWrite);
        fwrite(&mybuf[i]->scene, sizeof(long), 1, fileWrite);
        fwrite(&mybuf[i]->owl, sizeof(long), 1, fileWrite);
        fwrite(&mybuf[i]->flowers, sizeof(int), 1,fileWrite);
        fwrite(&mybuf[i]->pot, sizeof(char), 1, fileWrite);
        fwrite(&mybuf[i]->end, sizeof(unsigned char), 1, fileWrite);
        fwrite(&mybuf[i]->advertisement, sizeof(unsigned char), 1, fileWrite);
        fwrite(&mybuf[i]->bells, sizeof(int), 1, fileWrite);
        fwrite(&mybuf[i]->mountain, sizeof(unsigned char), 1, fileWrite);
        fwrite(&mybuf[i]->drop, sizeof(int), 1, fileWrite);
        fwrite(&mybuf[i]->muscle, sizeof(char[9]), 1, fileWrite);
        fwrite(&mybuf[i]->beginner, sizeof(unsigned int), 1, fileWrite);
        fwrite(&mybuf[i]->trucks, sizeof(float), 1, fileWrite);
        fwrite(&mybuf[i]->advice, sizeof(float), 1, fileWrite);
    }
    fclose(fileWrite);
}

//Compare function for qsort
//Input: pointers of two structs
int cmpFnc (const void *p1, const void *p2){
    struct stage4 * const *left = p1;
    struct stage4 * const *right = p2;

    int strCheck = strcmp((*left)->muscle, (*right)->muscle);

    if ((*left)->police > (*right)->police)
        return 1;
    else if ((*left)->police < (*right)->police)
        return -1;
    else if (strCheck > 0)
        return 1;
    else if (strCheck < 0)
        return -1;
    else if ((*left)->pot < (*right)->pot)
        return 1;
    else if ((*left)->pot > (*right)->pot)
        return -1;
    else if ((*left)->advice < (*right)->advice)
        return 1;
    else if ((*left)->advice > (*right)->advice)
        return -1;
    else if ((*left)->trucks < (*right)->trucks)
        return 1;
    else if ((*left)->trucks > (*right)->trucks)
        return -1;
    else if ((*left)->mountain < (*right)->mountain)
        return 1;
    else if ((*left)->mountain > (*right)->mountain)
        return -1;
    else if ((*left)->beginner < (*right)->beginner)
        return 1;
    else if ((*left)->beginner > (*right)->beginner)
        return -1;
    else if ((*left)->scene > (*right)->scene)
        return 1;
    else if ((*left)->scene < (*right)->scene)
        return -1;
    else if ((*left)->end < (*right)->end)
        return 1;
    else if ((*left)->end > (*right)->end)
        return -1;
    else if ((*left)->haircut > (*right)->haircut)
        return 1;
    else if ((*left)->haircut < (*right)->haircut)
        return -1;
    else if ((*left)->advertisement < (*right)->advertisement)
        return 1;
    else if ((*left)->advertisement > (*right)->advertisement)
        return -1;
    else if ((*left)->recess > (*right)->recess)
        return 1;
    else if ((*left)->recess < (*right)->recess)
        return -1;
    else if ((*left)->owl > (*right)->owl)
        return 1;
    else if ((*left)->owl < (*right)->owl)
        return -1;
    else if ((*left)->drop < (*right)->drop)
        return 1;
    else if ((*left)->drop > (*right)->drop)
        return -1;
    else if ((*left)->bells > (*right)->bells)
        return 1;
    else if ((*left)->bells < (*right)->bells)
        return -1;
    else if ((*left)->flowers > (*right)->flowers)
        return 1;
    else if ((*left)->flowers < (*right)->flowers)
        return -1;
    else return 0;
}


void usage()
{
    fprintf (stderr, "Usage: myprog infile outfile\n");
    fprintf (stderr, "-infile is a binary file\n-outfile is a binary file\n");
    exit(1); // Failure
}

int main(int argc, char * argv[]) {
    if (argc < 3){
       usage();
    }
    FILE *file = fopen(argv[1], "rb");
    if (!file){
      fprintf(stderr,"%s does not exist!!!\n", argv[1]);
      exit(1);
   }
   struct stat st;
   stat(argv[1], &st);
   struct stage4_input** input = (struct stage4_input**) malloc (st.st_size);
   int size = readFile(file, input);
   struct stage4** mybuf = (struct stage4**) malloc (size*sizeof(struct stage4*));
   convertInput(mybuf, input, size);
   //qsort(mybuf, size, sizeof(struct stage4*), cmpFnc);
   FILE *fileWrite = fopen(argv[2], "wb");
   writeFile(fileWrite, mybuf, size);
   free(mybuf);
   return 0;
}
