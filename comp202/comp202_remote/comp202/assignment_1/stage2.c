

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

struct stage2{
    char insurance[7];
    unsigned long park;
    int hospital;
    int wrist;
    char chance;
    unsigned char turkey;
    char corn;
    unsigned short suit;
    short int crack;
    unsigned char seed;
    double arch;
    int berry;
    double lunchroom;
    int advertisement;
    int doctor;
    float ink;
};
void printFields(){
    printf("insurance, ");
    printf("park, ");
    printf("hospital, ");
    printf("wrist, ");
    printf("chance, ");
    printf("turkey, ");
    printf("corn, ");
    printf("suit, ");
    printf("crack, ");
    printf("seed, ");
    printf("arch, ");
    printf("berry, ");
    printf("lunchroom, ");
    printf("advertisement, ");
    printf("doctor, ");
    printf("ink\n");
}

void printDetails(struct stage2 *player){
    printf("%s, " , player->insurance);
    printf("%lu, " , player->park);
    printf("%i, " , player->hospital);
    printf("%i, " , player->wrist);
    printf("%i, " , player->chance);
    printf("%i, " , player->turkey);
    printf("%c, " , player->corn);
    printf("%hu, " , player->suit);
    printf("%i, " , player->crack);
    printf("%i, " , player->seed);
    printf("%f, " , player->arch);
    printf("%i, ", player->berry);
    printf("%f , " , player->lunchroom);
    printf("%x ," , player->advertisement);
    printf("%i ," , player->doctor);
    printf("%f" , player->ink);
    printf("\n");
}

void usage()
{
    fprintf (stderr, "Usage: myprog infile\n");
    fprintf (stderr, "infile is a binary file\n");
    exit(1); // Failure
}

int main(int argc, const char * argv[]) {
    if (argc < 2){
        usage();
     }
    FILE *file = fopen(argv[1], "rb");
    if (!file){
        fprintf(stderr,"%s does not exist!!!\n", argv[1]);
        fclose(file);
        exit(1);
    }
    struct stage2 player;
    printFields();
    while (!feof(file)){
        fread(player.insurance, sizeof(char), 7, file);
        fread(&player.park, sizeof(unsigned long), 1, file);
        fread(&player.hospital, sizeof(int), 1, file);
        fread(&player.wrist, sizeof(int), 1, file);
        fread(&player.chance, sizeof(char), 1, file);
        fread(&player.turkey, sizeof(char), 1, file);
        fread(&player.corn, sizeof(char), 1, file);
        fread(&player.suit, sizeof(player.suit), 1, file);
        fread(&player.crack, sizeof(short int), 1, file);
        fread(&player.seed, sizeof(unsigned char), 1, file);
        fread(&player.arch, sizeof(double), 1, file);
        fread(&player.berry, sizeof(int), 1, file);
        fread(&player.lunchroom, sizeof(double), 1, file);
        fread(&player.advertisement, sizeof(int), 1, file);
        fread(&player.doctor, sizeof(int), 1, file);
        fread(&player.ink, sizeof(float), 1, file);
        if (feof(file) == 0){
            printDetails(&player);
        }
    }
    fclose(file);
    return 0;
}
