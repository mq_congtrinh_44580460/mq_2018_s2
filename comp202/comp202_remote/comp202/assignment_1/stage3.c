//
//  stage3.c
//  assignment_1
//
//  Created by Cong Anh Huan Trinh on 25/08/2016.
//  Copyright © 2016 Cong Anh Huan Trinh. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>

struct stage3{
    char insurance[7];
    unsigned long park;
    int hospital;
    int wrist;
    char chance;
    unsigned char turkey;
    char corn;
    unsigned short suit;
    short int crack;
    unsigned char seed;
    double arch;
    int berry;
    double lunchroom;
    int advertisement;
    int doctor;
    float ink;
};

void usage()
{
    fprintf (stderr, "Usage: myprog infile\n");
    fprintf (stderr, "infile is a binary file\n");
    exit(1); // Failure
}

void writeFile(const char *argv[], struct stage3 mybuf[], int i){
    FILE *fileWrite;
    fileWrite = fopen(argv[2],"w");
    fwrite(mybuf[i].insurance, sizeof(char), 7, fileWrite);
    fwrite(&mybuf[i].park, sizeof(unsigned long), 1, fileWrite);
    fwrite(&mybuf[i].hospital, sizeof(int), 1, fileWrite);
    fwrite(&mybuf[i].wrist, sizeof(int), 1, fileWrite);
    fwrite(&mybuf[i].chance, sizeof(char), 1, fileWrite);
    fwrite(&mybuf[i].turkey, sizeof(char), 1, fileWrite);
    fwrite(&mybuf[i].corn, sizeof(char), 1, fileWrite);
    fwrite(&mybuf[i].suit, sizeof(mybuf[i].suit), 1, fileWrite);
    fwrite(&mybuf[i].crack, sizeof(short int), 1, fileWrite);
    fwrite(&mybuf[i].seed, sizeof(unsigned char), 1, fileWrite);
    fwrite(&mybuf[i].arch, sizeof(double), 1, fileWrite);
    fwrite(&mybuf[i].berry, sizeof(int), 1, fileWrite);
    fwrite(&mybuf[i].lunchroom, sizeof(double), 1, fileWrite);
    fwrite(&mybuf[i].advertisement, sizeof(int), 1, fileWrite);
    fwrite(&mybuf[i].doctor, sizeof(int), 1, fileWrite);
    fwrite(&mybuf[i].ink, sizeof(float), 1, fileWrite);
}



void readFile(const char *argv[]){
    FILE *file = fopen(argv[1], "rb");
    if (!file){
        fprintf(stderr,"%s does not exist!!!\n", argv[1]);
        fclose(file);
        exit(1);
    }
    FILE *fileWrite;
     fileWrite = fopen(argv[2],"rb";
    struct stat st;
    unsigned long nRec = st.st_size/sizeof(struct stage3);
    struct stage3 *mybuf = (struct stage3*)malloc(nRec * sizeof(struct stage3));
    int i = 0;
    while (!feof(file)){
        fread(mybuf[i].insurance, sizeof(char), 7, file);
        fread(&mybuf[i].park, sizeof(unsigned long), 1, file);
        fread(&mybuf[i].hospital, sizeof(int), 1, file);
        fread(&mybuf[i].wrist, sizeof(int), 1, file);
        fread(&mybuf[i].chance, sizeof(char), 1, file);
        fread(&mybuf[i].turkey, sizeof(char), 1, file);
        fread(&mybuf[i].corn, sizeof(char), 1, file);
        fread(&mybuf[i].suit, sizeof(mybuf[i].suit), 1, file);
        fread(&mybuf[i].crack, sizeof(short int), 1, file);
        fread(&mybuf[i].seed, sizeof(unsigned char), 1, file);
        fread(&mybuf[i].arch, sizeof(double), 1, file);
        fread(&mybuf[i].berry, sizeof(int), 1, file);
        fread(&mybuf[i].lunchroom, sizeof(double), 1, file);
        fread(&mybuf[i].advertisement, sizeof(int), 1, file);
        fread(&mybuf[i].doctor, sizeof(int), 1, file);
        fread(&mybuf[i].ink, sizeof(float), 1, file);
        writeFile(argv, mybuf, i);
        fwrite(mybuf[i].insurance, sizeof(char), 7, fileWrite);
        fwrite(&mybuf[i].park, sizeof(unsigned long), 1, fileWrite);
         fwrite(&mybuf[i].hospital, sizeof(int), 1, fileWrite);
         fwrite(&mybuf[i].wrist, sizeof(int), 1, fileWrite);
         fwrite(&mybuf[i].chance, sizeof(char), 1, fileWrite);
         fwrite(&mybuf[i].turkey, sizeof(char), 1, fileWrite);
         fwrite(&mybuf[i].corn, sizeof(char), 1, fileWrite);
         fwrite(&mybuf[i].suit, sizeof(mybuf[i].suit), 1, fileWrite);
         fwrite(&mybuf[i].crack, sizeof(short int), 1, fileWrite);
         fwrite(&mybuf[i].seed, sizeof(unsigned char), 1, fileWrite);
         fwrite(&mybuf[i].arch, sizeof(double), 1, fileWrite);
         fwrite(&mybuf[i].berry, sizeof(int), 1, fileWrite);
         fwrite(&mybuf[i].lunchroom, sizeof(double), 1, fileWrite);
         fwrite(&mybuf[i].advertisement, sizeof(int), 1, fileWrite);
         fwrite(&mybuf[i].doctor, sizeof(int), 1, fileWrite);
         fwrite(&mybuf[i].ink, sizeof(float), 1, fileWrite);
        i++;
    }
    fclose(fileWrite);
    fclose(file);
}

int main(int argc, const char * argv[]) {
    if (argc < 2){
        usage();
    }
    readFile(argv);
    return 0;
}
