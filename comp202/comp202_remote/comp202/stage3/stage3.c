#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>
#include <string.h>

struct stage3{
    int recess;
    double haircut;
    short int police;
    long scene;
    long owl;
    int flowers;
    char pot;
    unsigned char end;
    unsigned char advertisement;
    int bells;
    unsigned char mountain;
    int drop;
    char muscle[9];
    unsigned int beginner;
    float trucks;
    float advice;
};

//Use to check the input and output
void printFile(struct stage3 *player){
    printf("%i, ", player->recess);
    printf("%f, ", player->haircut);
    printf("%d, ", player->police);
    printf("%li, ", player->scene);
    printf("%lx, ", player->owl);
    printf("%u, ", player->flowers);
    printf("%c, ", player->pot);
    printf("%i, ", player->end);
    printf("%i, ", player->advertisement);
    printf("%u, ", player->bells);
    printf("%i, ", player->mountain);
    printf("%i, ", player->drop);
    printf("%s, ", player->muscle);
    printf("%u, ", player->beginner);
    printf("%f, ", player->trucks);
    printf("%f\n", player->advice);
}

//Read file and return number of records.
int readFile(FILE *file, struct stage3 *mybuf[]){
    int count = 0;
    while (1){
        mybuf[count] = (struct stage3*)malloc(sizeof(struct stage3));
        fread(&mybuf[count]->recess, sizeof(int), 1, file);
        fread(&mybuf[count]->haircut, sizeof(double), 1, file);
        fread(&mybuf[count]->police, sizeof(short int), 1, file);
        fread(&mybuf[count]->scene, sizeof(long), 1, file);
        fread(&mybuf[count]->owl, sizeof(long), 1, file);
        fread(&mybuf[count]->flowers, sizeof(int), 1,file);
        fread(&mybuf[count]->pot, sizeof(char), 1, file);
        fread(&mybuf[count]->end, sizeof(unsigned char), 1, file);
        fread(&mybuf[count]->advertisement, sizeof(unsigned char), 1, file);
        fread(&mybuf[count]->bells, sizeof(int), 1, file);
        fread(&mybuf[count]->mountain, sizeof(unsigned char), 1, file);
        fread(&mybuf[count]->drop, sizeof(int), 1, file);
        fread(&mybuf[count]->muscle, sizeof(char[9]), 1, file);
        fread(&mybuf[count]->beginner, sizeof(unsigned int), 1, file);
        fread(&mybuf[count]->trucks, sizeof(float), 1, file);
        fread(&mybuf[count]->advice, sizeof(float), 1, file);

        if (feof(file)){
            break;
        }
        count++;
    }
    free(mybuf[count]);
    fclose(file);
    return count;
}

void writeFile(FILE *fileWrite, struct stage3 *mybuf[], int size){
    for (int i = 0 ; i < size ; i++){
        fwrite(&mybuf[i]->recess, sizeof(int), 1, fileWrite);
        fwrite(&mybuf[i]->haircut, sizeof(double), 1, fileWrite);
        fwrite(&mybuf[i]->police, sizeof(short int), 1, fileWrite);
        fwrite(&mybuf[i]->scene, sizeof(long), 1, fileWrite);
        fwrite(&mybuf[i]->owl, sizeof(long), 1, fileWrite);
        fwrite(&mybuf[i]->flowers, sizeof(int), 1,fileWrite);
        fwrite(&mybuf[i]->pot, sizeof(char), 1, fileWrite);
        fwrite(&mybuf[i]->end, sizeof(unsigned char), 1, fileWrite);
        fwrite(&mybuf[i]->advertisement, sizeof(unsigned char), 1, fileWrite);
        fwrite(&mybuf[i]->bells, sizeof(int), 1, fileWrite);
        fwrite(&mybuf[i]->mountain, sizeof(unsigned char), 1, fileWrite);
        fwrite(&mybuf[i]->drop, sizeof(int), 1, fileWrite);
        fwrite(&mybuf[i]->muscle, sizeof(char[9]), 1, fileWrite);
        fwrite(&mybuf[i]->beginner, sizeof(unsigned int), 1, fileWrite);
        fwrite(&mybuf[i]->trucks, sizeof(float), 1, fileWrite);
        fwrite(&mybuf[i]->advice, sizeof(float), 1, fileWrite);
    }
    fclose(fileWrite);
}

//Compare function for qsort
//Input: pointers of two structs
int cmpFnc (const void *p1, const void *p2){
    struct stage3 * const *left = p1;
    struct stage3 * const *right = p2;

    int strCheck = strcmp((*left)->muscle, (*right)->muscle);

    if ((*left)->police > (*right)->police)
        return 1;
    else if ((*left)->police < (*right)->police)
        return -1;
    else if (strCheck > 0)
        return 1;
    else if (strCheck < 0)
        return -1;
    else if ((*left)->pot < (*right)->pot)
        return 1;
    else if ((*left)->pot > (*right)->pot)
        return -1;
    else if ((*left)->advice < (*right)->advice)
        return 1;
    else if ((*left)->advice > (*right)->advice)
        return -1;
    else if ((*left)->trucks < (*right)->trucks)
        return 1;
    else if ((*left)->trucks > (*right)->trucks)
        return -1;
    else if ((*left)->mountain < (*right)->mountain)
        return 1;
    else if ((*left)->mountain > (*right)->mountain)
        return -1;
    else if ((*left)->beginner < (*right)->beginner)
        return 1;
    else if ((*left)->beginner > (*right)->beginner)
        return -1;
    else if ((*left)->scene > (*right)->scene)
        return 1;
    else if ((*left)->scene < (*right)->scene)
        return -1;
    else if ((*left)->end < (*right)->end)
        return 1;
    else if ((*left)->end > (*right)->end)
        return -1;
    else if ((*left)->haircut > (*right)->haircut)
        return 1;
    else if ((*left)->haircut < (*right)->haircut)
        return -1;
    else if ((*left)->advertisement < (*right)->advertisement)
        return 1;
    else if ((*left)->advertisement > (*right)->advertisement)
        return -1;
    else if ((*left)->recess > (*right)->recess)
        return 1;
    else if ((*left)->recess < (*right)->recess)
        return -1;
    else if ((*left)->owl > (*right)->owl)
        return 1;
    else if ((*left)->owl < (*right)->owl)
        return -1;
    else if ((*left)->drop < (*right)->drop)
        return 1;
    else if ((*left)->drop > (*right)->drop)
        return -1;
    else if ((*left)->bells > (*right)->bells)
        return 1;
    else if ((*left)->bells < (*right)->bells)
        return -1;
    else if ((*left)->flowers > (*right)->flowers)
        return 1;
    else if ((*left)->flowers < (*right)->flowers)
        return -1;
    else return 0;
}


void usage()
{
    fprintf (stderr, "Usage: myprog infile outfile\n");
    fprintf (stderr, "-infile is a binary file\n-outfile is a binary file\n");
    exit(1); // Failure
}

int main(int argc, char * argv[]) {
    if (argc < 3){
       usage();
    }
    FILE *file = fopen(argv[1], "rb");
    if (!file){
      fprintf(stderr,"%s does not exist!!!\n", argv[1]);
      exit(1);
   }
   struct stat st;
   stat(argv[1], &st);
   struct stage3** mybuf = (struct stage3**) malloc(st.st_size);
   printf("%ld", st.st_size);
   int size = readFile(file, mybuf);
   qsort(mybuf, size, sizeof(struct stage3*), cmpFnc);
   FILE *fileWrite = fopen(argv[2], "wb");
   writeFile(fileWrite, mybuf, size);
   free(mybuf);
   return 0;
}
