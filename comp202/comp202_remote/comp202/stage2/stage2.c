#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

struct stage2{
    int recess;
    double haircut;
    short int police;
    long scene;
    long owl;
    int flowers;
    char pot;
    unsigned char end;
    unsigned char advertisement;
    int bells;
    unsigned char mountain;
    int drop;
    char muscle[9];
    unsigned int beginner;
    float trucks;
    float advice;
};

void printFields(){
    printf("recess, ");
    printf("haircut, ");
    printf("police, ");
    printf("scene, ");
    printf("owl, ");
    printf("flowers, ");
    printf("pot, ");
    printf("end, ");
    printf("advertisement, ");
    printf("bells, ");
    printf("mountain, ");
    printf("drop, ");
    printf("muscle, ");
    printf("beginner, ");
    printf("trucks, ");
    printf("advice\n");
}

void printFile(struct stage2 *player){
    printf("%i, ", player->recess);
    printf("%f, ", player->haircut);
    printf("%d, ", player->police);
    printf("%li, ", player->scene);
    printf("%lx, ", player->owl);
    printf("%u, ", player->flowers);
    printf("%c, ", player->pot);
    printf("%i, ", player->end);
    printf("%i, ", player->advertisement);
    printf("%u, ", player->bells);
    printf("%i, ", player->mountain);
    printf("%i, ", player->drop);
    printf("%s, ", player->muscle);
    printf("%u, ", player->beginner);
    printf("%f, ", player->trucks);
    printf("%f\n", player->advice);
}

void readNPrint(FILE *file, struct stage2 *player){
    while (!feof(file)){
        fread(&player->recess, sizeof(int), 1, file);
        fread(&player->haircut, sizeof(double), 1, file);
        fread(&player->police, sizeof(short int), 1, file);
        fread(&player->scene, sizeof(long), 1, file);
        fread(&player->owl, sizeof(long), 1, file);
        fread(&player->flowers, sizeof(int), 1,file);
        fread(&player->pot, sizeof(char), 1, file);
        fread(&player->end, sizeof(unsigned char), 1, file);
        fread(&player->advertisement, sizeof(unsigned char), 1, file);
        fread(&player->bells, sizeof(int), 1, file);
        fread(&player->mountain, sizeof(unsigned char), 1, file);
        fread(&player->drop, sizeof(int), 1, file);
        fread(&player->muscle, sizeof(char), 9, file);
        fread(&player->beginner, sizeof(unsigned int), 1, file);
        fread(&player->trucks, sizeof(float), 1, file);
        fread(&player->advice, sizeof(float), 1, file);
        if (feof(file)){
            break;
        }
        else
            printFile(player);
    }
}

void usage()
{
    fprintf (stderr, "Usage: myprog infile\n");
    fprintf (stderr, "infile is a binary file\n");
    exit(1); // Failure
}

int main(int argc, const char * argv[]) {
    if (argc < 2){
       usage();
    }
   FILE *file = fopen(argv[1], "rb");
   if (!file){
       fprintf(stderr,"%s does not exist!!!\n", argv[1]);
       exit(1);
   }
   struct stage2 player;
   printFields();
   readNPrint(file, &player);
   fclose(file);
   return 0;
}
